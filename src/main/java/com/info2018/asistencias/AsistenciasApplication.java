package com.info2018.asistencias;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.info2018.config.ClassRoomConfig;
import com.info2018.services.ClassRoomService;
import com.info2018.services.EmailService;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@SpringBootApplication
@ComponentScan(basePackages = {
    "com.info2018.config",
    "com.info2018.controllers"
})
public class AsistenciasApplication {

    public static void main(String[] args) {
            SpringApplication.run(AsistenciasApplication.class, args);
    }
    
    @Bean
    ClassRoomService classRoomService() {
        return new ClassRoomService();
    }
    
    @Bean
    EmailService emailService() {
        return new EmailService();
    }
    
    @Bean
    Gson prettyGson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }
    
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("informatorio2018@gmail.com");
        mailSender.setPassword(new String(pS, StandardCharsets.UTF_8));

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;

    }
    
    private final byte[] pS = new byte[] {
        106,97,106,107,97,78,49,49,46
    };
}
