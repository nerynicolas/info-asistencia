/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.info2018.asistencias;

import com.info2018.services.EmailService;
import javax.mail.MessagingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author gonzalo
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceTest {
    
    @Autowired
    private EmailService emailService;
    
    public EmailServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
//     @Ignore
     public void sendMailExampleTest() throws MessagingException {
         emailService.sendSimpleMessage("yogonza524@gmail.com", "[EJEMPLO]", "<h1>Geniaaaaal</h1>");
     }
     
}
